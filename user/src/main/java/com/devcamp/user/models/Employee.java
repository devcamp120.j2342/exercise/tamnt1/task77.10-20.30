package com.devcamp.user.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "employees")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotEmpty(message = "Input last name")
	@Column(name = "last_name", length = 50)
	private String lastName;

	@NotEmpty(message = "Input first name")
	@Column(name = "first_name", length = 50)
	private String firstName;

	@NotEmpty(message = "Input extension")
	@Column(length = 50)
	private String extension;

	@NotEmpty(message = "Input email")
	@Column(unique = true, length = 50)
	private String email;

	@Min(message = "Input office code >= 0", value = 0)
	@Column(name = "office_code")
	private int officeCode;

	@Column(name = "report_to")
	private Integer reportTo;

	@NotEmpty(message = "Input job title")
	@Column(name = "job_title", length = 50)
	private String jobTitle;

	public Employee() {
	}

	public Employee(int id, String lastName, String firstName, String extension, String email, int officeCode,
			Integer reportTo, String jobTitle) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.extension = extension;
		this.email = email;
		this.officeCode = officeCode;
		this.reportTo = reportTo;
		this.jobTitle = jobTitle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(int officeCode) {
		this.officeCode = officeCode;
	}

	public int getReportTo() {
		return reportTo;
	}

	public void setReportTo(int reportTo) {
		this.reportTo = reportTo;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

}
