package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.user.models.ProductLine;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine, Integer> {

}
