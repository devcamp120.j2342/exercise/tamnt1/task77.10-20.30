package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.user.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

}
